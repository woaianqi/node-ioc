import { Result } from './Result';
import { delay, attemp } from './functions';
import { readConfigFile } from './loadConfigFile';
import rootPath from 'app-root-path';
export { Result, delay, attemp, readConfigFile, rootPath };
