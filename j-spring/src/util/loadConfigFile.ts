import YAML from 'yaml';
import path from 'path';
import appRootPath from 'app-root-path';
import fs from 'fs';

export function readConfigFile(filePath: string): any {
  const { ext } = path.parse(filePath);
  const finalPath = path.isAbsolute(filePath)
    ? filePath
    : appRootPath.resolve(filePath);
  const fileContent = fs.readFileSync(finalPath, {
    encoding: 'utf-8',
  });
  switch (ext) {
    case '.json':
      return JSON.parse(fileContent.toString());
      break;
    case '.yaml':
      return YAML.parse(fileContent);
      break;
    default:
      throw `file type error! only support json or yaml!`;
  }
}
