//延迟函数 用于方便调试
export function delay(time: number) {
  return new Promise(r => {
    setTimeout(r, time);
  });
}

//尝试次数
export const attemp = async <T>(
  fn: () => Promise<T>,
  count: number,
  opt?: {
    errorConsumer?: (e: any, time: number, count: number) => {};
    delay?: number;
  }
) => {
  for (let i = 0; i < count; i++) {
    try {
      return await fn();
    } catch (e) {
      if (opt?.errorConsumer) {
        opt.errorConsumer(e, i, count);
      }
    }
    if (opt?.delay) {
      await delay(opt.delay);
    }
  }
  throw `attemp final error!`;
};
