import {
  Component,
  SpringContainer,
  BeanDefine,
  spring,
  Logger,
  Autowired,
} from '../../src';

@Component()
class ApiController {
  hello = 'already instance';
}

@Component()
class Application extends SpringContainer {
  @Autowired()
  log: Logger;

  main() {
    this.log.info(`info日志`);
    this.log.warn(`warn日志`);
    this.log.http(`http日志`);
    this.log.error(`error日志`);

    let v = 'no find';

    this.getBeanDefineMap().forEach((bean: any, bd: BeanDefine) => {
      if (bd.clazz === ApiController) {
        v = bean['hello'];
      }
    });
    return v;
  }
}

/**
 * 'j-spring.log.level': 'info,http'
 * info以上包括warn和http都应该打印！
 *
 */
spring
  .loadConfig({ 'j-spring.log.level': 'warn,http' })
  .bind(ApiController)
  .launch(Application);
