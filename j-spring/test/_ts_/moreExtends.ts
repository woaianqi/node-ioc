import {
  SpringStarter,
  ClazzExtendsMap,
  Value,
  Autowired,
  Component,
  Logger,
  spring,
  Clazz,
} from '../../src';

async function main() {
  abstract class Person implements SpringStarter {
    @Value({ path: 'name' })
    name: string;

    @Value({ path: 'school' })
    school: string;

    @Autowired()
    log: Logger;

    getLogIsAutowired() {
      this.log.info(`注入成功`);
    }

    isSpringStater(): boolean {
      return true;
    }
    async doStart(_clazzMap: ClazzExtendsMap): Promise<any> {
      return this.getLogIsAutowired();
    }
  }

  @Component()
  class Student1 extends Person {}

  @Component()
  class Student2 extends Person {}

  @Component()
  class App {
    @Value({ path: 'test' })
    test: string;
  }

  await spring
    .loadConfig({
      'j-spring.log': { level: 'debug' },
      name: 'xiaoming',
      test: 'tmsg',
      school: '68zhong',
    })
    .bind(Student1, Student2, App)
    .invokeStarter();
}

main();
