import { SocketIoServerStarter } from './starter';
import {
  SocketHandlerPostProcessor,
  SocketHandler,
} from './socketHandlerPostProcessor';
import { spring } from 'j-spring';

export * from 'socket.io';
export { SocketHandler } from './socketHandlerPostProcessor';
export { SocketIoServerStarter } from './starter';

export const socketModule = (
  handler: (new () => SocketHandler<any, any>)[]
) => {
  spring.bindList(handler);
  return [SocketHandlerPostProcessor, SocketIoServerStarter];
};
