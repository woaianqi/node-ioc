import { spring } from 'j-spring';

export type ServiceAnnoParam = {
  allMethodProxy?: boolean;
};

export type TranscationParam = {
  isNewTx?: boolean; //启用新的事务
};

/**
 * 事务标记 标识该类将会被代理
 *
 */
export const Service = (p?: ServiceAnnoParam) =>
  spring.classAnnotationGenerator('j-spring-jpa.Service', p || {}, Service);

/**
 * 事务标记类或方法
 * 标记类：标识该类所有方法 都会添加事务
 * 标记方法：该方法的事务会添加注释
 */
export const Transactional = (p?: TranscationParam) =>
  spring.methodAnnotationGenerator(
    'j-spring-jpa.Transactional ',
    p || {},
    Transactional
  );
