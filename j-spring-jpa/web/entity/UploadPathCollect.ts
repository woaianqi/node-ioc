import { Column, Entity } from 'typeorm';
import { JpaEntity, JpaSearch } from '../../src';

@Entity({ name: 'UploadPathCollect', synchronize: false })
export class UploadPathCollect extends JpaEntity<UploadPathCollect> {
  @Column('longtext')
  path: string;

  @Column('int')
  isRemove: number;
}

export class UploadPathCollectSearch extends JpaSearch<
  UploadPathCollect,
  UploadPathCollectSearch
> {
  constructor() {
    super(UploadPathCollect);
  }

  path_fuzzy: string;
}
