import { JpaEntity, Column, Entity } from '../../src';

@Entity({ name: 'image', synchronize: false })
export class Image extends JpaEntity<Image> {
  @Column()
  name: string;
}
