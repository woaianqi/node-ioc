import { ApiRemark, Controller, Get, PathVariable } from 'j-spring-web';
import { SpringDao } from '../../src';
import { Autowired } from 'j-spring';
import { TableDataService } from '../service/TableDataService';

@Controller('/zcDataApi')
export class ZcDataApiController {
  @Autowired()
  springDao: SpringDao;

  @Autowired()
  tableDataService: TableDataService;

  @Get('/getTableList')
  @ApiRemark(`获取所有的表`)
  async getTableList() {
    return await this.tableDataService.showTable();
  }

  @Get('/getColumn/:colName')
  @ApiRemark(`获取字段信息`)
  async getColumn(@PathVariable('colName') colName: string) {
    return await this.tableDataService.showColumn(colName);
  }

  @Get('/getExistColmn')
  @ApiRemark(`获取可能存在文件的字段`)
  async getExistColmn() {
    return await this.tableDataService.getMaybeExistColumn();
  }

  @Get()
  @ApiRemark(`抽取所有可能存在路径的数据`)
  async loadAllPath() {
    return await this.tableDataService.loadAllMaybeFilePath();
  }
}
