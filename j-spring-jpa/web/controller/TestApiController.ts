import { Autowired, Logger } from 'j-spring';
import { ApiRemark, Controller, Get } from 'j-spring-web';
import { Service, Transactional, SpringDao, SpringSearchDao } from '../../src';
import { Image } from '../entity/Image';
import { Post, PostSearch } from '../entity/Post';

@Service()
class MsgProxy {
  a: number = 1;

  @Autowired()
  springDao: SpringDao;

  @Autowired()
  springSearchDao: SpringSearchDao;

  @Transactional({ isNewTx: true })
  async say(msg: string, n: number) {
    return 100;
  }

  @Transactional()
  async toTestTx() {
    const postList: Post[] = [];

    for (let i = 0; i < 5; i++) {
      const post = new Post().of({
        image: new Image().of({
          name: 'heloa',
        }),
        title: 'hello',
        likesCount: 100,
        text: 'a lot str',
      });
      postList.push(post);
    }

    await this.springDao.save(postList);

    await this.say('xx', 22);

    throw `sssshxxx`;

    return postList;
  }

  @Transactional()
  async toTestUpdatNew() {
    const ur = await this.springDao.updateProperty(
      Image,
      { name: 'xiha' },
      { name: 'hello' }
    );

    const image = new Image().of({
      name: 'hello,',
    });
    const i = await this.springDao.save(image);
    image.name += 'word!';
    await this.springDao.update(image);

    console.log('---------------查处来实体');

    const imageList = await this.springDao.queryEntity({
      clazz: Image,
    });
    imageList.forEach(im => (im.name += 'good!'));

    await this.springDao.update(imageList);

    const imageCount = await this.springDao.queryCount({ clazz: Image });
    console.log({ imageCount });

    const fastResult = await this.springDao.queryEntity({
      clazz: Image,
      where: { name: 'xx' },
      pagin: {
        pageSize: 20,
      },
      order: {
        name: 'ASC',
      },
      isEntity: true,
    });
    console.log({ fastResult });

    const fastCountt = await this.springDao.queryEntity({
      clazz: Image,
    });
    console.log({ fastCountt });
  }
}

@Controller('testApi')
export class TestApiController {
  @Autowired()
  log: Logger;

  @Autowired()
  springDao: SpringDao;

  @Autowired()
  msgProxy: MsgProxy;

  @Autowired()
  springSearchDao: SpringSearchDao;

  @ApiRemark('测试最新代码')
  @Get()
  async toTestUpdatNew() {
    await this.msgProxy.toTestUpdatNew();
    return '更新成通过';
  }

  @ApiRemark('打印消息')
  @Get()
  async toMsg() {
    return this.msgProxy.say('xxxx', 200);
  }

  @Get()
  async toTestTx(): Promise<Post[]> {
    return await this.msgProxy.toTestTx();
  }

  @Get()
  async toTestNew200() {
    const image = new Image().of({
      name: 'h123',
    });
    await this.springDao.save(image);
    const imageList = await this.springDao.queryEntity({
      clazz: Image,
      where: {
        name: 'h123',
      },
    });

    imageList.forEach(i => (i.name = `${i.name}!`));

    await this.springDao.update(imageList);

    const ims = await this.springDao.query<Image>(
      'select * from image where name=?',
      ['h123!']
    );

    const [fa] = await this.springDao.query<Image>(
      'select id,name from image  ',
      ['h123!#']
    );

    fa.name += '.';

    const count = await this.springDao.queryCount({
      clazz: Image,
      where: { name: 'h123!#.' },
    });

    return count;
  }

  @Get()
  async toTestNoTx() {
    for (let i = 0; i < 10; i++) {
      const p = new Post().of({
        image: new Image().of({
          name: 'heloa' + i,
        }),
        title: 'hello',
        likesCount: 100,
        text: 'a lot str',
      });
      await this.springDao.save(p);
      if (i == 5) {
        throw 'occur error!';
      }
    }

    return '已经添加';
  }

  @Get()
  async toUseSearch() {
    const s = new PostSearch().of({
      likesCount_not: -1,
      image$name_like: 'he%',
      pageSize: 5,
      likesCount_between: [-50, 2],
    });
    return await this.springSearchDao.find(s.usePagin());
  }

  @Get()
  async updateData() {
    const p = new Post().of({
      likesCount: -1,
    });
    await this.springDao.save(p);

    const s = new PostSearch().of({
      likesCount: -1,
    });

    const postList = await this.springSearchDao.find(s);

    postList.forEach(p => (p.likesCount = -1));

    await this.springDao.update(postList);

    return postList;
  }

  @Get()
  async batchUpdate() {
    const updateSize = await this.springSearchDao.batchUpdate(
      new PostSearch().of({
        image$name: 'heloa',
      }),
      {
        text: 'helalala',
      }
    );

    return updateSize;
  }

  @Get()
  async getPost() {
    const e = await this.springDao.getEntityManager();
    return await e.find(Post, {
      take: 5,
      where: {
        image: {
          name: 'heloa',
        },
      },
      relationLoadStrategy: 'query',
      relations: {
        image: true,
      },
      comment: 'this is test query',
      cache: 60 * 1000,
    });
  }
}
