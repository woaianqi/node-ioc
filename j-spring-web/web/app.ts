import { Component, spring } from 'j-spring';
import {
  baseWebConfiguration,
  springWebModule,
  ExpressMemorySessionConfiguration,
  SpringWebExceptionHandler,
  stopServer,
} from '../src';
import { errorInfo } from '../src';
import { ExpressServer } from '../src';
import { IndexController } from './controller/IndexController';
import { GetTestApi } from './controller/GetTestApi';
import { PostTestApi } from './controller/PostTestApi';
import path from 'path';
import { WinstonLog } from 'j-spring-log';

@Component()
class CustomSpringWebExceptionHandler implements SpringWebExceptionHandler {
  isSpringWebExceptionHandler(): boolean {
    return true;
  }
  hanlder(_req: any, res: any, errorInfo: errorInfo): void {
    console.log(`this is diy error info`);
    console.error(errorInfo);
    res.status(errorInfo.status).json(errorInfo);
  }
}

//SpringWeb 配置
const springWebConfig = [
  ...baseWebConfiguration, //基础配置
  ExpressMemorySessionConfiguration,
  CustomSpringWebExceptionHandler,
];

//请求控制器
const controllerClassList = [IndexController, GetTestApi, PostTestApi];

export async function start(port: number) {
  const config = {
    'j-spring': {
      log: {
        level: 'debug,http',
      },
    },
    'express.static': {
      prefix: '/static',
      path: path.join(__dirname, 'static'),
    },
    'j-spring-web': {
      port,
    },
    express: {
      view: {
        root: path.resolve(__dirname),
      },
    },
    indexMsg: 'j-spring',
  };
  await spring
    .loadConfig(config)
    .loadLogger(WinstonLog)
    .bindModule([springWebModule(controllerClassList), springWebConfig])
    .invokeStarter();
}

export function end(done: Function) {
  stopServer().then(() => done());
}
