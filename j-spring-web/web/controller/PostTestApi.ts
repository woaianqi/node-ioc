import {
  ApiRemark,
  Get,
  Post,
  Controller,
  RequestParam,
  ArrayString,
  PathVariable,
  validator,
  RequestBody,
} from '../../src';
import {} from 'class-validator';

class User {
  @validator.IsString()
  name: string;
  @validator.IsNumber()
  age: number;
}

@Controller('/PostTestApi')
export class PostTestApi {
  @Post('/postParam1')
  @ApiRemark(`测试整型的参数获取`)
  async postParam1(@RequestParam('a') a: number, @RequestParam('b') b: number) {
    return a + b;
  }

  @Post('/postParam2')
  @ApiRemark(`测试混合类型的参数获取`)
  async postParam2(@RequestParam('a') a: number, @RequestParam('b') b: string) {
    return `${a + 1}-${b}`;
  }

  @Post('/postParam3')
  @ApiRemark(`混合测试并且校验数据结构`)
  async postParam3(@RequestBody() stu: User) {
    return stu.name + stu.age;
  }

  @Post('/postParam4')
  @ApiRemark(`混合测试并且校验数据结构`)
  async postParam4(@RequestBody() stu: User) {
    return stu.name + stu.age;
  }
}
