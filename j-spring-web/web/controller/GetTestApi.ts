import {
  ApiRemark,
  Get,
  Controller,
  RequestParam,
  ArrayString,
  PathVariable,
} from '../../src';

@Controller('/getTestApi')
export class GetTestApi {
  @Get('/getParam1')
  @ApiRemark(`测试整型的参数获取`)
  async getParam1(@RequestParam('a') a: number, @RequestParam('b') b: number) {
    return a + b;
  }

  @Get('/getParam2')
  @ApiRemark(`测试字符串类型的参数获取`)
  async getParam2(@RequestParam('a') a: string, @RequestParam('b') b: string) {
    return a + b;
  }

  @Get('/getParam3')
  @ApiRemark(`测试混合类型的参数获取`)
  async getParam3(@RequestParam('a') a: number, @RequestParam('b') b: string) {
    return `${a + 1}${b}`;
  }

  @Get('/getParam4')
  @ApiRemark(`测试数组字符串类型的传入`)
  async getParam4(
    @RequestParam('a') a: ArrayString,
    @RequestParam('b') b: string
  ) {
    return `${a.join('-')}-${b}`;
  }

  @Get('/getParam5/:a/:b')
  @ApiRemark(`测试resful传值`)
  async getParam5(@PathVariable('a') a: number, @PathVariable('b') b: string) {
    return `${a + 100}-${b}`;
  }

  @Get('/getParam6/:a')
  @ApiRemark(`测试resful与query混合传值`)
  async getParam6(@PathVariable('a') a: number, @RequestParam('b') b: string) {
    return `${a + 100}-${b}`;
  }
}
