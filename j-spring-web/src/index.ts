import {
  ControllerBeanProcessor,
  ExpressAppEnhanceBeanProcessor,
  SpringParamterBeanPostProcessor,
} from './springWebBeanProcessor';
import { BodyParseConfiguration,EjsViewConfigruation,StaticResourceConfigruation,MorganLogConfigruation } from './springWebConfiguration'
import { SpringResultOperatePostProcessor } from './springWebBeanProcessor';
import { Clazz,spring } from 'j-spring';
import { ExpressServer } from './springReflectType'

export * as validator from 'class-validator';

import { SpringWebStarter } from './springWebContainer';

export {
  SpringWebExceptionHandler,
  isSpringWebExceptionHandler,
} from './springWebExtends';
export * from './springWebConfiguration';
export * from './springWebAnnotation';
export * from './springReflectType';
export * from './springWebExtends';
export {
  ParamEnhanceInterceptor,
  isParamEnhanceInterceptor,
} from './paramEnhanceInterceptor';
export {
  RouterEnhanceInterceptor,
  isRouterEnhanceInterceptor,
} from './routerEnhanceInterceptor';
export * from './paramEnhanceInterceptor';



/**
 * SpringWebStarter web启动器
 * ExpressAppEnhanceBeanProcessor ExpressConfiguration的后置处理器
 * ControllerBeanProcessor controller的后置处理器
 */
export function springWebModule(apiClassList:Clazz[]){
  spring.bindList(apiClassList);
  return [
    SpringWebStarter,
    ExpressAppEnhanceBeanProcessor,
    ControllerBeanProcessor,
    SpringParamterBeanPostProcessor,
    SpringResultOperatePostProcessor,
  ];
}


/**
 * 基础配置 
 */

export const baseWebConfiguration = [
  MorganLogConfigruation, //express日志转发到spring
  BodyParseConfiguration,//application解析

  EjsViewConfigruation,//ejs渲染
  StaticResourceConfigruation,//静态资源

]


export function end(done: Function) {
  const server = spring.getBeanFromContainer(ExpressServer);
  if (server) {
    server.close(e => {
      done(e);
    });
  }
}

//关闭服务
export function stopServer():Promise<void>{
  return new Promise((r,n) => {
    const server = spring.getBeanFromContainer(ExpressServer);
    if (server) {
      server.close(e => {
        e ? n(e):r()
      });
    }else{
      n('没有发现express实例')
    }
  });
}